package com.example.library.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookReturnDto;
import com.example.library.entity.Book;
import com.example.library.entity.BookBorrow;
import com.example.library.entity.BookReturn;
import com.example.library.entity.Status;
import com.example.library.entity.User;
import com.example.library.exception.BorrowIdNotFoundException;
import com.example.library.exception.ResourceConflictException;
import com.example.library.exception.UnauthorizedException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.repository.BookBorrowRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.BookReturnRepository;
import com.example.library.repository.UserRepository;

@ExtendWith(SpringExtension.class)
class BookReturnServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	BookBorrowRepository bookBorrowRepository;
	@Mock
	BookReturnRepository bookReturnRepository;
	@Mock
	BookRepository bookRepository;
	@InjectMocks
	BookReturnServiceImpl bookReturnServiceImpl;
	
	@Test
	void testReturnBookSuccessCase1() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		User user=User.builder().userId(1L).build();
		Book book=Book.builder().bookId(1L).noOfCopies(1).build();
		BookReturn bookReturn=BookReturn.builder().returnId(1L).build();
		BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book).user(user).borrowDate(LocalDate.parse("2023-05-09")).dueDate(LocalDate.parse("2023-05-09")).status(Status.APPROVED).build();
		Mockito.when(userRepository.findById(bookReturnDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(bookBorrowRepository.findById(bookReturnDto.borrowId())).thenReturn(Optional.of(bookBorrow));
		Mockito.when(bookBorrowRepository.findByBookBorrowIdAndUser(bookBorrow.getBookBorrowId(), user)).thenReturn(Optional.of(bookBorrow));
		Mockito.when( bookBorrowRepository.save(bookBorrow)).thenReturn(bookBorrow);
		Mockito.when(bookRepository.findById(bookBorrow.getBook().getBookId())).thenReturn(Optional.of(book));
		Mockito.when(bookReturnRepository.save(bookReturn)).thenReturn(bookReturn);
		Mockito.when(bookRepository.save(book)).thenReturn(book);
		
		ApiResponse apiResponse=bookReturnServiceImpl.returnBook(bookReturnDto);
		assertEquals("Book Returned Successfully", apiResponse.getMessage());
		
	}
	
	@Test
	void testReturnBookSuccessCase2() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		User user=User.builder().userId(1L).build();
		Book book=Book.builder().bookId(1L).noOfCopies(1).build();
		BookReturn bookReturn=BookReturn.builder().returnId(1L).build();
		BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book).user(user).borrowDate(LocalDate.parse("2024-03-04")).dueDate(LocalDate.parse("2023-05-09")).status(Status.APPROVED).build();
		Mockito.when(userRepository.findById(bookReturnDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(bookBorrowRepository.findById(bookReturnDto.borrowId())).thenReturn(Optional.of(bookBorrow));
		Mockito.when(bookBorrowRepository.findByBookBorrowIdAndUser(bookBorrow.getBookBorrowId(), user)).thenReturn(Optional.of(bookBorrow));
		Mockito.when( bookBorrowRepository.save(bookBorrow)).thenReturn(bookBorrow);
		Mockito.when(bookRepository.findById(bookBorrow.getBook().getBookId())).thenReturn(Optional.of(book));
		Mockito.when(bookReturnRepository.save(bookReturn)).thenReturn(bookReturn);
		Mockito.when(bookRepository.save(book)).thenReturn(book);
		
		ApiResponse apiResponse=bookReturnServiceImpl.returnBook(bookReturnDto);
		assertEquals("Book Returned Successfully", apiResponse.getMessage());
		
	}
	
	
	@Test
	void testReturnBookUserNotFound() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		Mockito.when(userRepository.findById(bookReturnDto.userId())).thenReturn(Optional.empty());
		assertThrows(UserNotFoundException.class, ()->bookReturnServiceImpl.returnBook(bookReturnDto));
		
	}
	
	@Test
	void testReturnBookBorrowIdNotFoundException() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		User user=User.builder().userId(1L).build();
		Mockito.when(userRepository.findById(bookReturnDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(bookBorrowRepository.findById(bookReturnDto.borrowId())).thenReturn(Optional.empty());
		assertThrows(BorrowIdNotFoundException.class, ()->bookReturnServiceImpl.returnBook(bookReturnDto));	
	}
	@Test
	void testReturnBookResourceConflictException() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		User user=User.builder().userId(1L).build();
		Book book=Book.builder().bookId(1L).noOfCopies(1).build();
		
		BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book).user(user).borrowDate(LocalDate.parse("2024-03-04")).dueDate(LocalDate.parse("2023-05-09")).status(Status.APPROVED).build();
		Mockito.when(userRepository.findById(bookReturnDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(bookBorrowRepository.findById(bookReturnDto.borrowId())).thenReturn(Optional.of(bookBorrow));
		Mockito.when(bookBorrowRepository.findByBookBorrowIdAndUser(bookBorrow.getBookBorrowId(), user)).thenReturn(Optional.empty());
		assertThrows(ResourceConflictException.class, ()->bookReturnServiceImpl.returnBook(bookReturnDto));
	}
	@Test
	void testReturnBookUnauthorizedException() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		User user=User.builder().userId(1L).build();
		Book book=Book.builder().bookId(1L).noOfCopies(1).build();
		BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book).user(user).borrowDate(LocalDate.parse("2023-05-09")).dueDate(LocalDate.parse("2023-05-09")).status(Status.PENDING).build();
		Mockito.when(userRepository.findById(bookReturnDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(bookBorrowRepository.findById(bookReturnDto.borrowId())).thenReturn(Optional.of(bookBorrow));
		Mockito.when(bookBorrowRepository.findByBookBorrowIdAndUser(bookBorrow.getBookBorrowId(), user)).thenReturn(Optional.of(bookBorrow));
		assertThrows(UnauthorizedException.class, ()->bookReturnServiceImpl.returnBook(bookReturnDto));
	}
	
	
	
}
