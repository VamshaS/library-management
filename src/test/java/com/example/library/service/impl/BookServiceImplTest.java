package com.example.library.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Optional;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.entity.Book;
import com.example.library.entity.BookBorrow;
import com.example.library.entity.Status;
import com.example.library.entity.User;
import com.example.library.exception.ResourceNotFoundException;
import com.example.library.repository.BookBorrowRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;

@ExtendWith(SpringExtension.class)
class BookServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	BookRepository bookRepository;
	@Mock
	BookBorrowRepository bookBorrowRepository;
	@InjectMocks
	BookServiceImpl bookServiceImpl;
	@Test
	void testSuccess1() {
		String bookName="OOPS";
        String authorName="Dr.Spoorthi";
        String category="2";
        Integer pageNumber=0;
        Integer pageSize=10; 
      
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        Book book=new Book(1L, bookName, pageSize, authorName, category);
        BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book).user(user).status(Status.APPROVED).build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookNameAndAuthorNameAndCategory(bookName, authorName, category)).thenReturn(Optional.of(book));
        bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId);
        Mockito.when(bookBorrowRepository.findByBookAndUser(book,user)).thenReturn(Optional.of(bookBorrow));
        assertNotNull(bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId));
	}
	
	@Test
	void testSuccess2() {
		String bookName="OOPS";
        String authorName="Dr.Spoorthi";
        String category="2";
        Integer pageNumber=0;
        Integer pageSize=10; 
      
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        Book book=new Book(1L, bookName, pageSize, authorName, category);
        
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookNameAndAuthorNameAndCategory(bookName, authorName, category)).thenReturn(Optional.of(book));
        bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId);
        Mockito.when(bookBorrowRepository.findByBookAndUser(book,user)).thenReturn(Optional.empty());
        assertNotNull(bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId));
	}
	
	@Test
	void testSuccess3() {
		String bookName="OOPS";
        String authorName=null;
        String category=null;
        Integer pageNumber=0;
        Integer pageSize=10; 
      
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        Book book=new Book(1L, bookName, pageSize, authorName, category);
        BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book).user(user).status(Status.APPROVED).build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookNameAndAuthorNameAndCategory(bookName, authorName, category)).thenReturn(Optional.of(book));
        bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId);
        Mockito.when(bookBorrowRepository.findByBookAndUser(book,user)).thenReturn(Optional.of(bookBorrow));
        assertNotNull(bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId));
	}
	
	@Test
	void testSuccess4() {
		String bookName="OOPS";
        String authorName=null;
        String category=null;
        Integer pageNumber=0;
        Integer pageSize=10; 
        Pageable pageable=PageRequest.of(pageNumber, pageSize);
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        Book book1=new Book(1L, bookName, pageSize, authorName, category);
        Book book2=new Book(1L, bookName, pageSize, authorName, category);
       
        BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book1).user(user).status(Status.APPROVED).build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookName(bookName,pageable)).thenReturn(Arrays.asList(book1,book2));
        bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId);
        Mockito.when(bookBorrowRepository.findByBookAndUser(book1,user)).thenReturn(Optional.of(bookBorrow));
        assertNotNull(bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId));
	}
	
	@Test
	void testSuccess5() {
		String bookName="OOPS";
        String authorName="Dr.Spoorthi";
        String category=null;
        Integer pageNumber=0;
        Integer pageSize=10; 
        Pageable pageable=PageRequest.of(pageNumber, pageSize);
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        Book book1=new Book(1L, bookName, pageSize, authorName, category);
        Book book2=new Book(1L, bookName, pageSize, authorName, category);
       
        BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book1).user(user).status(Status.APPROVED).build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookNameAndAuthorName(bookName, authorName,pageable)).thenReturn(Arrays.asList(book1,book2));
        bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId);
        Mockito.when(bookBorrowRepository.findByBookAndUser(book1,user)).thenReturn(Optional.of(bookBorrow));
        assertNotNull(bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId));
	}
	
	@Test
	void testSuccess6() {
		String bookName="OOPS";
        String authorName=null;
        String category="2";
        Integer pageNumber=0;
        Integer pageSize=10; 
        Pageable pageable=PageRequest.of(pageNumber, pageSize);
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        Book book1=new Book(1L, bookName, pageSize, authorName, category);
        Book book2=new Book(1L, bookName, pageSize, authorName, category);
       
        BookBorrow bookBorrow=BookBorrow.builder().bookBorrowId(1L).book(book1).user(user).status(Status.APPROVED).build();
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookNameAndCategory(bookName, category,pageable)).thenReturn(Arrays.asList(book1,book2));
        bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId);
        Mockito.when(bookBorrowRepository.findByBookAndUser(book1,user)).thenReturn(Optional.of(bookBorrow));
        assertNotNull(bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId));
	}
	
	@Test
	void test() {
		String bookName="OOPS";
        String authorName="Dr.Spoorthi";
        String category="2";
        Integer pageNumber=0;
        Integer pageSize=10; 
      
        Long userId=1L;
        User user=User.builder().userId(userId).build();
        
      
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        Mockito.when(bookRepository.findByBookNameAndAuthorNameAndCategory(bookName, authorName, category)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class,()->bookServiceImpl.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId) );
	}
}
