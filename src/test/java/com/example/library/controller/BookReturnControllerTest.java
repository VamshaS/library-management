package com.example.library.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookReturnDto;
import com.example.library.service.BookReturnService;

@ExtendWith(SpringExtension.class)
class BookReturnControllerTest {
	@Mock
	BookReturnService bookReturnService;
	@InjectMocks
	BookReturnController bookReturnController;
	@Test
	void testReturnBook() {
		BookReturnDto bookReturnDto=new BookReturnDto(1L, 1L);
		Mockito.when(bookReturnService.returnBook(bookReturnDto)).thenReturn(new ApiResponse("Book Returned Successfully", 201L));
		ResponseEntity<ApiResponse> responseEntity=bookReturnController.returnBook(bookReturnDto);
		assertEquals(201L, responseEntity.getBody().getStatus());
	}
}
