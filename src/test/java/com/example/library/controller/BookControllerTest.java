package com.example.library.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyList;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.library.dto.SearchBookDto;
import com.example.library.service.BookService;

@ExtendWith(SpringExtension.class)
class BookControllerTest {
	@Mock
	BookService bookService;
	
	@InjectMocks
	BookController bookController;
	@Test
	void testsearchBook() {
		String bookName="OOPS";
        String authorName="Dr.Spoorthi";
        String category="2";
        Integer pageNumber=0;
        Integer pageSize=10; 
        Long userId=1L;
        
        Mockito.when(bookService.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId))
        .thenReturn(Arrays.asList(new SearchBookDto(bookName, authorName, category, category, LocalDate.now())));
        
        ResponseEntity<List<SearchBookDto>> responseEntity=bookController.searchBook(bookName, authorName, category, pageNumber, pageSize, userId);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	
	}

}
