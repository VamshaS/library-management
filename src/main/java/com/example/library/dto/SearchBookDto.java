package com.example.library.dto;

import java.time.LocalDate;

import com.example.library.entity.Status;

import lombok.Builder;

@Builder
public record SearchBookDto(String bookName,String authorName,String category,String status,LocalDate dueDate) {

}
