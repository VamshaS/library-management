package com.example.library.dto;

import com.example.library.entity.Status;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookBorrowApprovalDto {
private Long userId;
private Long borroeid;
private Status status;
}
