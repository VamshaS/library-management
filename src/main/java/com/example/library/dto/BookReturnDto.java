package com.example.library.dto;


public record BookReturnDto(Long borrowId,Long userId) {

}
