package com.example.library.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.dto.SearchBookDto;
import com.example.library.service.BookService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class BookController {
	
	private final BookService bookService;
	
	@GetMapping("/books")
	public ResponseEntity<List<SearchBookDto>> searchBook(@RequestParam String bookName,@RequestParam(required = false)  String authorName,@RequestParam(required = false) String category,@RequestParam Integer pageNumber,
			@RequestParam Integer pageSize,@RequestParam Long userId){
		return  new ResponseEntity<>(bookService.searchBooks(bookName, authorName, category, pageNumber, pageSize, userId),HttpStatus.OK);
	}
}
