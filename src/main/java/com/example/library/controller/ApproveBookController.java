package com.example.library.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookBorrowApprovalDto;
import com.example.library.service.ApproveBookService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ApproveBookController {

	private final ApproveBookService bookReturnService;
	
	@PostMapping("/api/v1/book-aproval")
	ResponseEntity<ApiResponse> returnBook(@RequestBody BookBorrowApprovalDto bookReturnDto){
	return ResponseEntity.status(HttpStatus.OK).body(bookReturnService.returnBooks(bookReturnDto));
	}
	
}
