package com.example.library.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookReturnDto;
import com.example.library.service.BookReturnService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class BookReturnController {
	private final BookReturnService bookReturnService;
	
	@PostMapping("/books-return")
	public ResponseEntity<ApiResponse> returnBook(@RequestBody BookReturnDto bookReturnDto){
		return new ResponseEntity<>(bookReturnService.returnBook(bookReturnDto),HttpStatus.CREATED);
	}
}
