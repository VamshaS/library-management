package com.example.library.service;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookBorrowApprovalDto;

public interface ApproveBookService {

	ApiResponse returnBooks(BookBorrowApprovalDto bookBorrowApprovalDto);

}
