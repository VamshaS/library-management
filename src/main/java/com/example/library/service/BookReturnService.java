package com.example.library.service;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookReturnDto;

public interface BookReturnService {
	ApiResponse returnBook(BookReturnDto bookReturnDto);
}
