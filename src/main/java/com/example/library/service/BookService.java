package com.example.library.service;

import java.util.List;

import com.example.library.dto.SearchBookDto;

public interface BookService {
	List<SearchBookDto> searchBooks(String bookName,String authorName,String category,Integer pageNumber,Integer pageSize,Long userId);
}
