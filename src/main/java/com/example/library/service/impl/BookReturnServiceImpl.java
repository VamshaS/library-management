package com.example.library.service.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Service;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookReturnDto;
import com.example.library.entity.Book;
import com.example.library.entity.BookBorrow;
import com.example.library.entity.BookReturn;
import com.example.library.entity.Status;
import com.example.library.entity.User;
import com.example.library.exception.BorrowIdNotFoundException;
import com.example.library.exception.ResourceConflictException;
import com.example.library.exception.UnauthorizedException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.repository.BookBorrowRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.BookReturnRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.BookReturnService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookReturnServiceImpl implements BookReturnService{
	private final UserRepository userRepository;
	private final BookBorrowRepository bookBorrowRepository;
	private final BookReturnRepository bookReturnRepository;
	private final BookRepository bookRepository;
	
	@Override
	public ApiResponse returnBook(BookReturnDto bookReturnDto) {
		User user=userRepository.findById(bookReturnDto.userId()).orElseThrow(UserNotFoundException::new);
		BookBorrow bookBorrow =bookBorrowRepository.findById(bookReturnDto.borrowId()).orElseThrow(BorrowIdNotFoundException::new);
		if(bookBorrowRepository.findByBookBorrowIdAndUser(bookBorrow.getBookBorrowId(), user).isEmpty()) {
			log.warn("ResourceConflictException");
			throw new ResourceConflictException();
		
		}
		if(bookBorrow.getStatus().equals(Status.APPROVED)) {
			log.info("Calculate charge");
			double charge=0;
			long daysDifference=ChronoUnit.DAYS.between(LocalDate.now(), bookBorrow.getDueDate());
			if(daysDifference<=3 && daysDifference>0)
				charge=20;
			else if(daysDifference>3)
				charge=50;
			BookReturn bookReturn=new BookReturn();
			bookReturn.setCharge(charge);
			bookReturn.setReturnDate(LocalDate.now());
			bookReturn.setBookBorrow(bookBorrow);
			bookBorrow.setStatus(Status.RETURNED);
		    Book book=bookRepository.findById(bookBorrow.getBook().getBookId()).get();
		    book.setNoOfCopies(book.getNoOfCopies()+1);
		    bookBorrowRepository.save(bookBorrow);
			bookReturnRepository.save(bookReturn);
			bookRepository.save(book);
			log.info("Book Returned Successfully");
			return new ApiResponse("Book Returned Successfully", 201L);		
		}
		log.warn("Verify borrowId");
		throw new UnauthorizedException("Verify borrowId");
	}

}
