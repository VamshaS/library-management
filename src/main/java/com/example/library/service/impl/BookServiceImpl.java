package com.example.library.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.library.dto.SearchBookDto;
import com.example.library.entity.Book;
import com.example.library.entity.BookBorrow;
import com.example.library.entity.User;
import com.example.library.exception.ResourceNotFoundException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.repository.BookBorrowRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.BookService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookServiceImpl implements BookService {
	
	private final UserRepository userRepository;
	private final BookRepository bookRepository;
	private final BookBorrowRepository bookBorrowRepository;
	private static  String notBorrowed="Not_Borrow";
	
	@Override
	public List<SearchBookDto> searchBooks(String bookName, String authorName, String category, Integer pageNumber,
			Integer pageSize, Long userId) {
		User user=userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
		Pageable pageable=PageRequest.of(pageNumber, pageSize);
		if(Objects.isNull(authorName) && Objects.isNull(category)) {
			log.info("authorName And category is null");
			List<Book> books=bookRepository.findByBookName(bookName,pageable);
			
			return books.stream().map(book->{
				Optional<BookBorrow> bookBorrow=bookBorrowRepository.findByBookAndUser(book,user);
				if(bookBorrow.isPresent()) 
					return new SearchBookDto(bookName, book.getAuthorName(), book.getCategory(), bookBorrow.get().getStatus().name(), bookBorrow.get().getDueDate());
				return new SearchBookDto(bookName, book.getAuthorName(), book.getCategory(), notBorrowed, null);		
			}).toList();	
		}
		
		else if(Objects.isNull(authorName)) {
			log.info("category is null");
			List<Book> books=bookRepository.findByBookNameAndCategory(bookName, category,pageable);
			return books.stream().map(book->{
				Optional<BookBorrow> bookBorrow=bookBorrowRepository.findByBookAndUser(book,user);
				if(bookBorrow.isPresent()) 
					return new SearchBookDto(bookName, book.getAuthorName(), book.getCategory(), bookBorrow.get().getStatus().name(), bookBorrow.get().getDueDate());
				return new SearchBookDto(bookName, book.getAuthorName(), book.getCategory(), notBorrowed, null);		
			}).toList();
		}
		
		else if(Objects.isNull(category)) {
			log.info("authorName  is null");
			List<Book> books=bookRepository.findByBookNameAndAuthorName(bookName, authorName,pageable);
			return books.stream().map(book->{
				Optional<BookBorrow> bookBorrow=bookBorrowRepository.findByBookAndUser(book,user);
				if(bookBorrow.isPresent()) 
					return new SearchBookDto(bookName, book.getAuthorName(), book.getCategory(), bookBorrow.get().getStatus().name(), bookBorrow.get().getDueDate());
				return new SearchBookDto(bookName, book.getAuthorName(), book.getCategory(), notBorrowed, null);		
			}).toList();
		}
		
		Optional<Book> book=bookRepository.findByBookNameAndAuthorNameAndCategory(bookName, authorName, category);
		if(book.isEmpty())
			throw new ResourceNotFoundException();
		Optional<BookBorrow> bookBorrow=bookBorrowRepository.findByBookAndUser(book.get(),user);		
		if(bookBorrow.isPresent()) 
			return Arrays.asList(new SearchBookDto(bookName, authorName, category, bookBorrow.get().getStatus().name(), bookBorrow.get().getDueDate()));
		return Arrays.asList(new SearchBookDto(bookName, authorName, category, notBorrowed, null));
	}
	
	
}
