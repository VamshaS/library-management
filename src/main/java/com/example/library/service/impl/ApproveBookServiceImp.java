package com.example.library.service.impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.library.dto.ApiResponse;
import com.example.library.dto.BookBorrowApprovalDto;
import com.example.library.entity.Book;
import com.example.library.entity.BookBorrow;
import com.example.library.entity.Role;
import com.example.library.entity.Status;
import com.example.library.entity.User;
import com.example.library.exception.BorrowIdNotFoundException;
import com.example.library.exception.UnauthorizedException;
import com.example.library.exception.UserNotFoundException;
import com.example.library.repository.BookBorrowRepository;
import com.example.library.repository.BookRepository;
import com.example.library.repository.UserRepository;
import com.example.library.service.ApproveBookService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ApproveBookServiceImp implements ApproveBookService {
	private final UserRepository userRepository;
	private final BookBorrowRepository bookBorrowRepository;
	private final BookRepository bookRepository;

	@Override
	public ApiResponse returnBooks(BookBorrowApprovalDto bookBorrowApprovalDto) {
		User user = userRepository.findById(bookBorrowApprovalDto.getUserId())
				.orElseThrow(() -> new UserNotFoundException("This user was not exsit"));
		BookBorrow bookBorrow = bookBorrowRepository.findById(bookBorrowApprovalDto.getBorroeid())
				.orElseThrow(() -> new BorrowIdNotFoundException("Borrow id not exist"));
		if(user.getRole().equals(Role.USER)){
			throw new UnauthorizedException ("This user won't aprove book Requests");
		}
		else {
			bookBorrow.setStatus(bookBorrowApprovalDto.getStatus());
			bookBorrowRepository.save(bookBorrow);
			if(bookBorrow.getStatus().equals(Status.APPROVED)) {
				Long bookid = bookBorrow.getBook().getBookId();
				Optional<Book> book = bookRepository.findById(bookid);
				Book b1 =book.get();
				b1.setNoOfCopies(b1.getNoOfCopies()-1);
				bookRepository.save(b1);
			}
		}

		return ApiResponse.builder().status(200l).message("Leave status updated successfully").build();
	}

}
