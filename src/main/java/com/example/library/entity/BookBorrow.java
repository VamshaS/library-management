package com.example.library.entity;

import java.time.LocalDate;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookBorrow {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long bookBorrowId;
	@OneToOne(cascade = CascadeType.ALL)
	private Book book;
	@ManyToOne(cascade = CascadeType.ALL)
	private User user;
	private Status status;
	private LocalDate borrowDate;
	private LocalDate dueDate;
}
