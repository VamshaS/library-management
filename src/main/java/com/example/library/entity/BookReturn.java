package com.example.library.entity;

import java.time.LocalDate;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BookReturn {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long returnId;
	@OneToOne(cascade = CascadeType.ALL)
	private BookBorrow bookBorrow;
	private LocalDate returnDate;
	private Double charge;
}
