package com.example.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.library.entity.BookReturn;

public interface BookReturnRepository extends JpaRepository<BookReturn, Long> {

}
