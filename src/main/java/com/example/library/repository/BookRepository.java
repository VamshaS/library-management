package com.example.library.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.library.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long>{
	Optional<Book> findByBookNameAndAuthorNameAndCategory(String bookName,String authorName,String category);
	List<Book> findByBookName(String bookName,Pageable pageable);
	List<Book> findByBookNameAndAuthorName(String bookName,String authorName,Pageable pageable);
	List<Book> findByBookNameAndCategory(String bookName,String category,Pageable pageable);
}
