package com.example.library.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.library.entity.Book;
import com.example.library.entity.BookBorrow;
import com.example.library.entity.User;

public interface BookBorrowRepository extends JpaRepository<BookBorrow, Long> {
	Optional<BookBorrow> findByBookAndUser(Book book,User user);
	Optional<BookBorrow> findByBookBorrowIdAndUser(Long bookBorrowId,User user);
}
