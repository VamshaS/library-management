package com.example.library.exception;


public class BorrowIdNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public BorrowIdNotFoundException(String message) {
		super(message,GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}
	public BorrowIdNotFoundException() {
		super("Borrow Id Not Found exception",GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
