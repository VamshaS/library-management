package com.example.library.exception;


public class UnauthorizedException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public UnauthorizedException(String message) {
		super(message,GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}
	public UnauthorizedException() {
		super("Unauthorized",GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}


}
