package com.example.library.exception;

public class ResourceNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ResourceNotFoundException(String message) {
		super(message,GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}
	public ResourceNotFoundException() {
		super("ResourceNotFoundException",GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
